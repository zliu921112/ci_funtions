def call(dockerRepoName, imageName) {
    pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                sh 'pip install -r requirements.txt'
            }
        }
        stage('Python Lint') { 
            steps { 
                sh 'pylint-fail-under --fail_under 3.5 *.py'
            }
        }
        stage('Testing') {
            steps {
                script {
                    def files = findFiles(glob: "test*.py")
                    for (file in files) {
                        sh 'coverage run --omit */site-packages/*,*/dist-packages/* '+ file.path
                    }
                }
            }
            post {
                always {
                    script {
                        def find_test_reports = findFiles(glob: "test*.py")
                        if (find_test_reports) {                        
                            junit 'test-reports/*.xml'
                        }
                    }
                    sh 'coverage report'
                }
                
            }
        }
        stage('Docker') {
            when {
                expression { env.GIT_BRANCH == 'origin/master' }
                }
                    steps {
                        sh "docker login -u 'acit4850group8' -p '85a80ca4-e543-4c66-a630-3f4d4468a299' docker.io"
                        sh "docker build -t ${dockerRepoName}:latest --tag acit4850group8/${dockerRepoName}:${imageName} ."
                        sh "docker push acit4850group8/${dockerRepoName}:${imageName}"
                }
        }
        stage('Zip Artifacts') {
            steps {
                sh "zip app.zip *.py"
                archiveArtifacts artifacts: 'app.zip', fingerprint: true
            }
        }
    }
}

}